package main

import "math"

// Vector2D object
type Vector2D struct {
	x float64
	y float64
}

// Add function
func (v1 Vector2D) Add(v2 Vector2D) Vector2D {
	return Vector2D{v1.x + v2.x, v1.y + v2.y}
}

// Sub function
func (v1 Vector2D) Sub(v2 Vector2D) Vector2D {
	return Vector2D{v1.x - v2.x, v1.y - v2.y}
}

// Multiply function
func (v1 Vector2D) Multiply(v2 Vector2D) Vector2D {
	return Vector2D{v1.x * v2.x, v1.y * v2.y}
}

// AddV - adds velocity
func (v1 Vector2D) AddV(d float64) Vector2D {
	return Vector2D{v1.x * d, v1.y * d}
}

// MultiplyV - multiplies velocity
func (v1 Vector2D) MultiplyV(d float64) Vector2D {
	return Vector2D{v1.x * d, v1.y * d}
}

// DivideV - divides velocity
func (v1 Vector2D) DivideV(d float64) Vector2D {
	return Vector2D{v1.x / d, v1.y / d}
}

func (v1 Vector2D) limit(lower, upper float64) Vector2D {
	return Vector2D{math.Min(math.Max(v1.x, lower), upper),
		math.Min(math.Max(v1.y, lower), upper)}
}

// Distance - returns the distance betwen 2 objects
func (v1 Vector2D) Distance(v2 Vector2D) float64 {
	return math.Sqrt(math.Pow(v1.x-v2.x, 2) + math.Pow(v1.y-v2.y, 2))
}
