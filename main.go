package main

import (
	"embed"
	"fmt"
	"log"
	"math/rand"
	"time"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/audio"
	"github.com/tinne26/etxt"
)

type Mode int

const (
	screenWidth, screenHeight = 1024, 768
	gameTitle                 = "GoPong"
	author                    = "Amandreth"
	sampleRate                = 44100

	Splash Mode = iota
	Menu
	Play
	Settings
	NewGame
	LoadGame
	GameOver
	Quit
)

var (
	//go:embed resources
	resourceRootFS embed.FS

	//go:embed resources/fonts/Gugi-Regular.ttf
	gugiTtf []byte

	gugiFont *etxt.Font

	debug       bool
	acceptInput bool
	newGame     bool
	newBall     bool
	paused      bool
	mute        bool
	hit         bool

	audioContext *audio.Context
	volume       = 20

	gameMap [screenWidth + 41][screenHeight + 41]int
	s1      = rand.NewSource(time.Now().UnixNano())
	r1      = rand.New(s1)
)

// Game is the game structure
type Game struct {
	txtRenderer   *etxt.Renderer
	currentMode   Mode
	previousMode  Mode
	currentScreen *ebiten.Image
	count         int
}

func init() {
	audioContext = audio.NewContext(sampleRate)

	f, _, err := etxt.ParseFontBytes(gugiTtf)
	check(err)
	gugiFont = f

	debug = false
	paused = false
	acceptInput = true
	mute = false
	hit = false
}

func (g *Game) updateMode(m Mode) {
	g.previousMode = g.currentMode
	g.currentMode = m
}

func (g *Game) Update() error {
	g.count++

	if ebiten.IsKeyPressed(ebiten.KeyQ) {
		if g.currentMode != Quit {
			g.updateMode(Quit)
		}
	}

	if ebiten.IsKeyPressed(ebiten.KeyD) {
		if acceptInput {
			acceptInput = false
			debug = !debug
			go inputTimer()
		}
	}

	if ebiten.IsKeyPressed(ebiten.KeyEscape) {
		switch g.currentMode {
		case Settings:
			g.updateMode(g.previousMode)
		default:
			g.updateMode(Settings)
		}
	}

	return nil
}

func (g *Game) Draw(screen *ebiten.Image) {

	switch g.currentMode {
	case Splash:
		g.SplashScreen()
	case Menu:
		g.MenuScreen()
	case NewGame:
		g.NewGameScreen()
	case Play:
		//g.PlayScreen()
	case Settings:
		//g.SettingsScreen()
	case GameOver:
		//g.gameoverScreen()
	case Quit:
		g.QuitGameScreen()
	default:
	}

	if debug {
		g.debugOverlay()
	}

	op := &ebiten.DrawImageOptions{}
	screen.DrawImage(g.currentScreen, op)
}

// Layout something
func (g *Game) Layout(_, _ int) (w, h int) {
	return screenWidth, screenHeight
}

func main() {
	fmt.Printf("Device scale factor: %0.2f\n", ebiten.DeviceScaleFactor())

	w, h := ebiten.ScreenSizeInFullscreen()
	fmt.Printf("Screen size in fullscreen: %d, %d\n", w, h)

	ebiten.SetWindowSize(screenWidth, screenHeight)
	//ebiten.SetWindowSize(w, h)
	ebiten.SetWindowTitle(gameTitle)
	ebiten.SetWindowResizable(false)
	ebiten.SetWindowResizingMode(ebiten.WindowResizingModeOnlyFullscreenEnabled)

	cache := etxt.NewDefaultCache(1024 * 1024 * 1024) // 1GB cache
	renderer := etxt.NewStdRenderer()
	renderer.SetCacheHandler(cache.NewHandler())

	if err := ebiten.RunGame(&Game{txtRenderer: renderer, currentMode: Splash, previousMode: Splash, currentScreen: ebiten.NewImage(screenWidth, screenHeight)}); err != nil {
		log.Fatal(err)
	}
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func inputTimer() {
	time.Sleep(300 * time.Millisecond)
	acceptInput = true
}

func hitTimer() {
	PlaySE(paddleWav)
	time.Sleep(300 * time.Millisecond)
	hit = false
}

func resetGameMap() {
	for y := 0; y < screenWidth+41; y++ {
		for z := 0; z < screenHeight+41; z++ {
			gameMap[y][z] = 0
		}
	}
}
