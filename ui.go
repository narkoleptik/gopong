package main

import (
	"fmt"
	"image/color"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/tinne26/etxt"
	"golang.org/x/image/colornames"
)

var (
	quarterScreenBox      *ebiten.Image
	halfScreenBox         *ebiten.Image
	threeQuarterScreenBox *ebiten.Image
)

func init() {
	quarterScreenBox = ebiten.NewImage(screenWidth/4, screenHeight/4)
	halfScreenBox = ebiten.NewImage(screenWidth/2, screenHeight/2)
	threeQuarterScreenBox = ebiten.NewImage(screenWidth*0.75, screenHeight*0.75)
}

// DropShadow drops a shadow
func (g *Game) DropShadow(text string, x, y, size int, fore, back color.RGBA, fnt *etxt.Font) {
	drop := 1
	g.txtRenderer.SetSizePx(size)
	g.txtRenderer.SetFont(fnt)
	g.txtRenderer.SetColor(back)
	g.txtRenderer.Draw(text, x+drop, y+drop)
	g.txtRenderer.SetColor(fore)
	g.txtRenderer.Draw(text, x, y)
}

func customBox(x, y int) *ebiten.Image {
	return ebiten.NewImage(x, y)
}

func (g *Game) debugOverlay() {

	fnt := gugiFont
	size := 24
	textColor := colornames.White
	text := fmt.Sprintf("TPS: %0.2f\nClock: %d\nMode: %d\nBalls: %d\nHit: %t", ebiten.ActualTPS(), g.count, g.currentMode, len(balls), hit)

	myBox := customBox(150, 150) //rect.Width.Ceil(), rect.Height.Ceil())
	myBox.Clear()
	//myBox.Fill(colornames.Pink)
	bw, bh := myBox.Size()
	g.txtRenderer.SetTarget(myBox)
	g.txtRenderer.SetAlign(etxt.Top, etxt.Left)
	g.txtRenderer.SetSizePx(size)
	g.txtRenderer.SetFont(fnt)
	g.txtRenderer.SetColor(textColor)
	g.txtRenderer.Draw(text, 5, 5) //screenWidth-rect.Width.Ceil(), screenHeight-rect.Height.Ceil())
	boxOP := &ebiten.DrawImageOptions{}
	boxOP.GeoM.Translate(float64(screenWidth-bw), float64(screenHeight-bh))
	g.currentScreen.DrawImage(myBox, boxOP)
}
