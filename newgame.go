package main

import (
	"fmt"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/vector"
	"github.com/tinne26/etxt"
	"golang.org/x/image/colornames"
)

// Player is the player
type Player struct {
	ID    int
	Score int
	X     int
	Y     int
	W     int
	H     int
}

var (
	players []Player
	offset  = 32
	speed   = 8
	balls   = make(map[int]*Ball)
)

// NewGameScreen is the splash screen
func (g *Game) NewGameScreen() {
	//foreColor := colornames.Yellow
	//backColor := colornames.Darkgray

	resetGameMap()
	g.currentScreen.Clear()
	g.currentScreen.Fill(colornames.Black)
	if newGame {
		newGame = false
		newBall = true
		p := Player{
			ID:    1,
			Score: 0,
			X:     offset,
			Y:     screenHeight/2 - offset,
			W:     offset,
			H:     offset * 4,
		}
		players = append(players, p)

		p = Player{
			ID:    2,
			Score: 0,
			X:     screenWidth - offset*2,
			Y:     screenHeight/2 - offset,
			W:     offset,
			H:     offset * 4,
		}
		players = append(players, p)

		//createBall(Vector2D{screenWidth / 4, screenHeight / 4})
		//createBall(Vector2D{screenWidth / 4, float64(r1.Intn(screenHeight))})

	}

	// Draw Score
	scoreText := fmt.Sprintf("%02d           %02d", players[0].Score, players[1].Score)
	g.txtRenderer.SetTarget(g.currentScreen)
	g.txtRenderer.SetAlign(etxt.YCenter, etxt.XCenter)
	g.txtRenderer.SetSizePx(96)
	//g.DropShadow("Game Menu", screenWidth/2, screenHeight/4, 96*2, foreColor, backColor, dumFont)
	//	dot := g.txtRenderer.Draw("Game Menu", screenWidth/2-rect.Width.Ceil()/2, screenHeight/2)
	rect := g.txtRenderer.SelectionRect(scoreText)
	myBox := customBox(rect.Width.Ceil(), rect.Height.Ceil())
	myBox.Clear()
	boxOP := &ebiten.DrawImageOptions{}
	bw, bh := myBox.Size()
	g.txtRenderer.SetColor(colornames.White)
	g.txtRenderer.SetTarget(myBox)
	g.txtRenderer.SetAlign(etxt.YCenter, etxt.XCenter)
	g.txtRenderer.Draw(scoreText, bw/2, bh/2)
	boxOP.GeoM.Translate(screenWidth/2-float64(bw/2), float64(bh/4)) //screenHeight/2-float64(bh/2))
	g.currentScreen.DrawImage(myBox, boxOP)

	// Draw Paddles
	for _, player := range players {
		//fmt.Println(player.ID, player.X, player.Y)
		for y := 0; y < player.H; y++ {
			for x := 0; x < player.W; x++ {
				g.currentScreen.Set(player.X+x, player.Y+y, colornames.White)
				gameMap[player.X+x][player.Y+y] = player.ID
			}
		}
	}

	// Draw ball
	ballSize = 20
	if newBall {
		fmt.Println("Balls:", len(balls))

		newBall = false
		for b := range balls {
			delete(balls, b)
		}
		createBall(Vector2D{screenWidth / 4, screenHeight / 4})
	}
	for _, b := range balls {
		vector.DrawFilledCircle(g.currentScreen, float32(b.position.x), float32(b.position.y), float32(ballSize), colornames.White, true)
	}

	// Player 1 movement
	if ebiten.IsKeyPressed(ebiten.KeyArrowLeft) || ebiten.IsKeyPressed(ebiten.KeyA) {
		players[0].Y -= speed
		if players[0].Y < 4 {
			players[0].Y = 4
		}
	}
	if ebiten.IsKeyPressed(ebiten.KeyArrowRight) || ebiten.IsKeyPressed(ebiten.KeyZ) {
		players[0].Y += speed
		if players[0].Y > screenHeight-players[0].H-4 {
			players[0].Y = screenHeight - players[0].H - 4
		}
	}

	// Player 2 movement
	if ebiten.IsKeyPressed(ebiten.KeyArrowUp) || ebiten.IsKeyPressed(ebiten.KeyK) {
		players[1].Y -= speed
		if players[1].Y < 4 {
			players[1].Y = 4
		}
	}
	if ebiten.IsKeyPressed(ebiten.KeyArrowDown) || ebiten.IsKeyPressed(ebiten.KeyM) {
		players[1].Y += speed
		if players[1].Y > screenHeight-players[1].H-4 {
			players[1].Y = screenHeight - players[1].H - 4
		}
	}
	/*
		if ebiten.IsKeyPressed(ebiten.Key1) {
			players[0].Score++
		}
		if ebiten.IsKeyPressed(ebiten.Key2) {
			players[1].Score++
		}
	*/

}
