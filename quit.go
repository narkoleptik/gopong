package main

import (
	"os"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/tinne26/etxt"
	"golang.org/x/image/colornames"
)

// QuitGameScreen is the splash screen
func (g *Game) QuitGameScreen() {
	g.currentScreen.Fill(colornames.Black)

	boxOP := &ebiten.DrawImageOptions{}
	quarterScreenBox.Clear()
	bw, bh := quarterScreenBox.Size()
	quitText := "Quit? (y/N)"
	g.txtRenderer.SetTarget(quarterScreenBox)
	g.txtRenderer.SetSizePx(32)
	g.txtRenderer.SetColor(colornames.Yellow)
	g.txtRenderer.SetAlign(etxt.YCenter, etxt.XCenter)
	g.txtRenderer.Draw(quitText, bw/2, bh/2)
	boxOP.GeoM.Translate(screenWidth/2-float64(bw/2), screenHeight/2-float64(bh/2))
	g.currentScreen.DrawImage(quarterScreenBox, boxOP)

	if ebiten.IsKeyPressed(ebiten.KeyY) {
		os.Exit(0)
	}
	if ebiten.IsKeyPressed(ebiten.KeyN) ||
		ebiten.IsKeyPressed(ebiten.KeyEscape) ||
		ebiten.IsKeyPressed(ebiten.KeyEnter) {
		g.updateMode(g.previousMode)
	}
}
