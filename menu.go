package main

import (
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/tinne26/etxt"
	"golang.org/x/image/colornames"
	"golang.org/x/image/math/fixed"
)

var (
	fontSize = 0
	boxMove  = 0
)

// MenuScreen is the menu screen
func (g *Game) MenuScreen() {
	foreColor := colornames.Yellow
	//backColor := colornames.Darkgray

	g.currentScreen.Clear()
	g.currentScreen.Fill(colornames.Black)

	boxOP := &ebiten.DrawImageOptions{}

	menuTitle := "Game Menu"
	g.txtRenderer.SetSizePx(96)
	rect := g.txtRenderer.SelectionRect(menuTitle)
	myBox := customBox(rect.Width.Ceil(), rect.Height.Ceil())
	myBox.Clear()
	bw, bh := myBox.Size()
	g.txtRenderer.SetTarget(g.currentScreen)
	g.txtRenderer.SetAlign(etxt.YCenter, etxt.XCenter)

	g.txtRenderer.SetColor(foreColor)
	g.txtRenderer.Draw(menuTitle, screenWidth/2, bh) //screenWidth-rect.Width.Ceil(), screenHeight-rect.Height.Ceil())

	boxOP.GeoM.Translate(screenWidth/2-float64(bw/2), float64(bh*2))
	g.currentScreen.DrawImage(myBox, boxOP)
	// Game MENU title bar

	// Menu Text
	boxOP.GeoM.Reset()
	menuText := " N)ew Game \n Q)uit"
	menuFontSize := 64
	g.txtRenderer.SetSizePx(menuFontSize)
	rect = g.txtRenderer.SelectionRect(menuText)
	myBox = customBox(rect.Width.Ceil(), rect.Height.Ceil())
	bw, bh = myBox.Size()
	myBox.Clear()
	g.txtRenderer.SetTarget(myBox)
	g.txtRenderer.SetAlign(etxt.Top, etxt.Left)
	g.txtRenderer.SetTarget(myBox)
	g.txtRenderer.SetAlign(etxt.Top, etxt.Left)
	g.txtRenderer.SetColor(colornames.Gray)

	// Change colors
	g.txtRenderer.Traverse(menuText, fixed.P(10, 10),
		func(dot fixed.Point26_6, codePoint rune, glyphIndex etxt.GlyphIndex) {
			if codePoint == '\n' {
				return
			} // skip line break

			if codePoint == ')' {
				g.txtRenderer.SetColor(colornames.Darkred)
			} else {
				g.txtRenderer.SetColor(colornames.Gray)
			}

			mask := g.txtRenderer.LoadGlyphMask(glyphIndex, dot)
			g.txtRenderer.DefaultDrawFunc(dot, mask, glyphIndex)

		})
	boxOP.GeoM.Translate(screenWidth/2-float64(bw/2+boxMove), screenHeight/2+96/2-float64(bh/2))
	g.currentScreen.DrawImage(myBox, boxOP)

	if ebiten.IsKeyPressed(ebiten.KeyN) {
		newGame = true
		g.updateMode(NewGame)
	}
}
