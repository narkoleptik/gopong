package main

import (
	"fmt"

	"github.com/tinne26/etxt"
	"golang.org/x/image/colornames"
)

// SplashScreen is the splash screen
func (g *Game) SplashScreen() {
	foreColor := colornames.Yellow
	backColor := colornames.Darkgray

	g.currentScreen.Clear()
	g.currentScreen.Fill(colornames.Black)
	g.txtRenderer.SetTarget(g.currentScreen)
	g.txtRenderer.SetAlign(etxt.YCenter, etxt.XCenter)

	g.DropShadow(gameTitle, screenWidth/2, screenHeight/4, 64*3, foreColor, backColor, gugiFont)
	g.DropShadow(fmt.Sprintf("by\n%s", author), screenWidth/2, screenHeight-96/2-10, 64/2, colornames.Lime, backColor, gugiFont)

	if g.count > 120 {
		g.updateMode(Menu)
	}
}
