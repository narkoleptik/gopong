package main

import (
	"fmt"
	"math"
	"time"
)

var (
	ballCount = 3
	ballSpeed = 3.0
	ballSize  = 5
)

// Ball is the adversary
type Ball struct {
	id       int
	position Vector2D
	velocity Vector2D
	angle    float64
	//sprite   Sprite
	speed float64
	alive bool
}

func createBall(p Vector2D) {
	e := Ball{
		position: p,
		id:       ballCount,
		speed:    ballSpeed, //float64(r1.Intn(1) + 1),
		alive:    true,
	}

	diffx, diffy := e.position.x-(screenWidth/2), e.position.y-(screenHeight/2)
	thetaRad := math.Atan2(diffy, diffx)
	e.angle = (thetaRad + math.Pi) * 360.0 / (2.0 * math.Pi)
	horizontalVelocity := e.speed * math.Cos(e.angle*float64(math.Pi/180.0))
	verticalVelocity := e.speed * math.Sin(e.angle*float64(math.Pi/180.0))
	e.velocity = Vector2D{horizontalVelocity, verticalVelocity}
	//e.sprite = ballSprites[r1.Intn(len(ballSprites))]
	//balls = append(balls, &e) //balls[0] = &e
	balls[e.id] = &e
	go e.start()
}

func (e *Ball) start() {
	for {
		e.moveOne()
		time.Sleep(10 * time.Millisecond)
	}
}

func (e *Ball) moveOne() {
	if e.alive {
		if !hit {
			// Check for hit on 4 compass points
			if e.position.x+float64(ballSize) < float64(screenWidth-ballSize) &&
				e.position.y+float64(ballSize) < float64(screenHeight-ballSize) &&
				e.position.x-+float64(ballSize) > 0 &&
				e.position.y-float64(ballSize) > 0 {
				if gameMap[int(e.position.x)][int(e.position.y-float64(ballSize))] != 0 {
					// North
					e.velocity.y *= -1
					hit = true
					go hitTimer()
				} else if gameMap[int(e.position.x)][int(e.position.y+float64(ballSize))] != 0 {
					// South
					e.velocity.y *= -1
					hit = true
					go hitTimer()
				} else if gameMap[int(e.position.x-float64(ballSize))][int(e.position.y)] != 0 {
					// East
					e.velocity.x *= -1 //= math.Pi - e.velocity.x
					hit = true
					go hitTimer()
				} else if gameMap[int(e.position.x+float64(ballSize))][int(e.position.y)] != 0 {
					// West
					e.velocity.x *= -1 //= math.Pi - e.velocity.x
					hit = true
					go hitTimer()
				}
			}
		}

		//fmt.Println(e.velocity.x, e.velocity.y)
		e.position = e.position.Add(e.velocity)
		if e.position.y >= screenHeight-float64(ballSize) {
			//fmt.Println("BOTTOM WALL")
			//e.velocity.y = (math.Pi * 2) - e.velocity.y
			e.velocity.y *= -1
		}
		if e.position.y <= float64(ballSize) {
			//fmt.Println("TOP WALL")
			e.velocity.y *= -1
			//e.velocity.y = (math.Pi * 2) - e.velocity.y
		}
		// verticalVelocity := e.speed * math.Sin(angle*float64(math.Pi/180.0))
		if e.position.x >= screenWidth {
			fmt.Println("RIGHT WALL", e.position.x, e.velocity.x)
			//TODO: destroy ball
			//e.velocity.x *= -1 //= math.Pi - e.velocity.x
			//delete(balls, e.id)
			e.alive = false
			//e.position = Vector2D{100, 100}
			PlaySE(lostWav)
			newBall = true
			players[0].Score++
			//createBall(Vector2D{screenWidth / 4, screenHeight / 4})
			//createBall(Vector2D{screenWidth / 4, float64(r1.Intn(screenHeight))})
		}
		if e.position.x <= 0 {
			fmt.Println("LEFT WALL", e.position.x, e.velocity.x)
			//ball.angle = Math.PI - ball.angle;
			//TODO: destroy ball
			//e.velocity.x *= -1 //= math.Pi - e.velocity.x
			//delete(balls, e.id)
			e.alive = false
			//e.position = Vector2D{100, 100}
			PlaySE(lostWav)
			newBall = true
			players[1].Score++
			//createBall(Vector2D{screenWidth / 8, screenHeight / 4})
			//createBall(Vector2D{screenWidth / 4, float64(r1.Intn(screenHeight))})
		}
	}
}
