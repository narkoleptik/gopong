package main

import (
	"bytes"
	"embed"
	"io/ioutil"
	"log"

	"github.com/hajimehoshi/ebiten/v2/audio"
	"github.com/hajimehoshi/ebiten/v2/audio/wav"
)

var (
	// SoundsRootFS sounds
	//go:embed resources/sounds
	SoundsRootFS embed.FS

	//go:embed resources/sounds/pong-paddle
	paddleWav []byte

	//go:embed resources/sounds/pong-ball-lost
	lostWav []byte
)

// PlaySE is the sound effect player
func PlaySE(bs []byte) {
	PlaySEwithVolume(bs, float64(volume/2)*.1)
}

// PlaySEwithVolume is the sound effect player
func PlaySEwithVolume(bs []byte, vol float64) {
	s, err := wav.DecodeWithSampleRate(sampleRate, bytes.NewReader(bs))
	if err != nil {
		log.Fatal(err)
		return
	}
	b, err := ioutil.ReadAll(s)
	if err != nil {
		log.Fatal(err)
		return
	}
	sePlayer := audio.NewPlayerFromBytes(audioContext, b)
	// sePlayer is never GCed as long as it plays.
	if !mute {
		sePlayer.SetVolume(vol)
		sePlayer.Play()
	}
}
